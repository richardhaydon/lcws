#ifndef RAW_BUFFER_HPP
#define RAW_BUFFER_HPP

#include <array>
#include <lcws_constants.hpp>
#include <lcws_strong_type.hpp>
#include <bitset>


namespace lcws {

/**
 * A Raw buffer is an object that holds a single block of “raw” samples. A Raw
 * buffer is filled with data from the sensor hardware unmodified as the sensor
 * produced the data. The samples and the contents of a raw buffer is therefore
 * sensor specific. The Raw buffer object shall only be used within the sound
 * system sub component
 */
using raw_buffer = std::array<uint8_t, constants::bytes_per_buffer>;

/**
 * @brief raw_sample_audio
 * @param rb raw buffer to take the value from
 * @param idx zero based index into an array of samples
 * @return 24-bit sample value to a 32-bit value masking away the accelerometer
 * data
 */
int32_t raw_sample_audio(const raw_buffer &rb, size_t idx);

/**
 * @brief raw_sample_accelerometer
 * @param rb raw buffer to take the value from
 * @param idx zero based index into an array of samples
 * @return a 24-bit sample value to a 6-bit accelerometer value masking away the
 * audio data
 */
int32_t raw_sample_accelerometer(const raw_buffer &rb, size_t idx);

/**
 * @brief verify_integrity
 * @param rb raw buffer to take the value from
 * @return true if the buffer rb fullfills the criteria for integrity
 */
bool verify_integrity(const raw_buffer &rb);

/**
 * @brief first_buffer_after_sensor_reset
 * @param rb raw buffer the operation applies to
 * @return true if the buffer has been marked as a first buffer after a sensor
 * reset
 */
bool first_buffer_after_sensor_reset(const raw_buffer &rb);

} // namespace lcws

#endif // RAW_BUFFER_HPP
