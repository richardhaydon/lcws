#ifndef LCWS_CONSTANTS_HPP
#define LCWS_CONSTANTS_HPP

namespace lcws
{
namespace constants
{
const unsigned bytes_per_sample = 3u;
const unsigned int framesize = 2048;
const unsigned bytes_per_buffer = framesize * bytes_per_sample;
const unsigned int num_rails = 2;
const unsigned int num_sensors = 4;
const unsigned int samples_per_group = 8;
}
};

#endif // LCWS_CONSTANTS_HPP
