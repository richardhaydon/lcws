#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <raw_buffer.hpp>
#include <array>
#include <lcws_constants.hpp>
#include <lcws_strong_type.hpp>

/*!
  A buffer to hold sound samples from a single channel.
  - The samples are in floating point format (the "float" type).
  - The samples are normalized in the range <-1.0..1.0>.
  */

class buffer {
public:

  buffer();
};

#endif // BUFFER_HPP
