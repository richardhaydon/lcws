#include "raw_buffer.hpp"
#include <bitset>
#include <array>

namespace lcws {

namespace {

inline int32_t sign_extend(const unsigned int value, const unsigned int width) {
  // sign extend trick. See
  // https://graphics.stanford.edu/~seander/bithacks.html#VariableSignExtend

  // Create a bitmask for the sign bit
  const int32_t m = static_cast<int32_t>(1U << (width - 1));
  // XOR bitwise of the mask and the value
  //(we flip the sign bit)
  const int32_t bits = value ^ m;
  // Obtain the result
  return bits - m;
  // If the number were positive then we flipped sign bit to 1 and the
  // substraction set it back to 0 If the number were negative then we flipped
  // sign bit to 0 and the substraction will propagate sign bit
}


// Reset flag in the buffer definition
const auto reset_bit = (20*8)+0;

/**
 * @brief byte_index maps reference to a sample by it's index number to the per
 * byte index for access into the buffer
 * @param sample_index
 * @return index into byte buffer corresponding to the sample index
 */
size_t constexpr to_byte_index(size_t sample_index) {
  return sample_index * constants::bytes_per_sample;
}

}

int32_t raw_sample_audio(const raw_buffer &rb, size_t idx)
{
    auto byte_idx = to_byte_index(idx);
    const auto c0 = static_cast<unsigned>(rb.at(byte_idx++));
    const auto c1 = static_cast<unsigned>(rb.at(byte_idx++));
    const auto c2 = static_cast<unsigned>(rb.at(byte_idx++));
    const unsigned int raw =
        static_cast<unsigned int>((c2 >> 6) | (c1 << 2) | (c0 << 10));
    const int signed_value = sign_extend(raw, 18);
    return signed_value;
}

int32_t raw_sample_accelerometer(const raw_buffer &rb, size_t idx)
{
    // Masking away the 2 audio-bits that is part of the byte with the accelerometer data
    return rb.at( to_byte_index(idx)+2 ) & 0x3f;
}

bool verify_integrity(const raw_buffer &rb)
{
    return true;
}

bool first_buffer_after_sensor_reset(const raw_buffer &rb)
{
    return false;
}

// namespace

} // namespace lcws
