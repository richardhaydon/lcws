#ifndef SAMPLE_PREPROCESSOR_H
#define SAMPLE_PREPROCESSOR_H

#include <system_error>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

class SamplePreprocessor
{
public:

SamplePreprocessor();

public:
/**
 * @brief load_full_recording
 * @param f full qualified path to the samples in raw full format
 * @return error code
 */
std::error_code load_full_recording(fs::path fp) ;



};

#endif // SAMPLE_PREPROCESSOR_H
