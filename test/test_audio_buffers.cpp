#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>

#include <cstdint>
#include <cstring>
#include <lcws_constants.hpp>
#include <raw_buffer.hpp>
#include <named_type.hpp>
#include <functional>
#include <vector>
#include "spdlog/spdlog.h"

using testing::_;

// repesenting the sized signed values in the buffer from the sensor board
struct Sample {
  int sound : 18;
  unsigned accel : 6;
};

// a sample group is a sequence of 8 samples, following the content definition from table 3
using sample_group = std::vector<Sample>;


/**
 * @brief sample_bytes generates the three bytes comprising a sample given the audio level and the following six bytes of the accelerometer level
 * @param sound_level the 18 bit sound level for the sample
 * @param acceler_6bits  six bit field for the accelerometer or other values from table 3
 * @return array of the bytes in order for inclusion in the sample block
 */
std::array<uint8_t,3> sample_bytes(int sound_level, unsigned acceler_6bits) {
    std::array<uint8_t,3> result;
    Sample s{sound_level,acceler_6bits};
    uint8_t *raw =  reinterpret_cast<uint8_t *>(&s);//   (uint8_t *)&s;
    result[0] = static_cast<uint8_t>((raw[2] << 6 | raw[1] >> 2) );
    result[1] = static_cast<uint8_t>((raw[0] >> 2) | (raw[1] << 6));
    result[2] = static_cast<uint8_t>((raw[0] & 0x03) << 6) + ( (raw[2]>>2)& 0x3f  );

    return result;
}

/**
 * @brief form_sample_group
 * @param levels construct as per table three a set of (8) samples  array of sound levels in the sample group
 * @param acc_x accelerometer, signed 8 bit value
 * @param acc_y accelerometer, signed 8 bit value
 * @param acc_z accelerometer, signed 8 bit value
 * @param acc_fs accelerometer full scale defelections in two bits
 * @param temperature internal temperature snsor reading from the sensor
 * @param counter six bit counter of bytes sent within a block
 * @param reset normally zero, set for the first sample group in a block if the SRU uC has reset.
 * @return byte vector representing the mapped bytes for the group of samples given
 */
std::vector<uint8_t> form_sample_group( std::array<int,lcws::constants::samples_per_group> levels,
                               int8_t acc_x, int8_t acc_y, int int8_t,
                               uint8_t acc_fs,
                               uint8_t temperature,
                               uint8_t counter,
                               bool reset
                                )

{
    std::vector<uint8_t> result;
    // sound 0 and the top six bits of x-acc                          -- offset 0
    std::array<uint8_t,3> sb = sample_bytes(levels[0], (acc_x>>2));
    result.insert(result.end(),sb.begin(),sb.end() );
    // sound 1 low 2 bits of x, fs1, fs0, and top two bits of y       -- offset 3
    sb = sample_bytes(levels[1], (acc_y >> 6) | ( (acc_fs & 0x03) << 2) | ( (acc_x&0x03) << 4 )    );
    result.insert(result.end(),sb.begin(),sb.end() );
    // sound 2 and upper six bits of  accel y                         -- offset 6
//    std::array<uint8_t,3> sb = sample_bytes(levels[2], (acc_y>>2));
    result.insert(result.end(),sb.begin(),sb.end() );

    return  result;

}


// Create a test fixture to handle common setup and teardown between the tests
class AudioBufferFixture : public ::testing::Test {
public:
  AudioBufferFixture();
  ~AudioBufferFixture();
};

// Test that the mail slot names generated for instances are as expected
TEST_F(AudioBufferFixture, raw_buffer_constructor) {
    spdlog::info("Starting the audio buffer tests");
  lcws::raw_buffer b{0};
  // brittle test of the buffer size based on sample size in bytes and the
  // number of samples in a buffer
  EXPECT_EQ(b.size(), 3u * 2048);
  // currently only checks the zero bytes, which will pass
  EXPECT_TRUE(lcws::verify_integrity(b));
}

TEST_F(AudioBufferFixture, assignment) {
  uint8_t r[lcws::constants::bytes_per_buffer];
  lcws::raw_buffer rb;
  std::copy(std::begin(r), std::end(r), std::begin(rb));
}


// Rapid check property assertion test of decoding sound and accelerometer values from samples
RC_GTEST_PROP(raw_buffer_access, decode_sample, ( )) {
    lcws::raw_buffer rb{0};

    const auto acc_level = *rc::gen::inRange<unsigned>(0, 63);
    const auto snd_level = *rc::gen::inRange<int>(-131072,131071 );
    auto samp_bytes = sample_bytes(snd_level, acc_level);
    std::copy(std::begin(samp_bytes),std::end(samp_bytes),std::begin(rb));
    RC_ASSERT(lcws::raw_sample_audio(rb, 0) == snd_level);
    RC_ASSERT(lcws::raw_sample_accelerometer(rb, 0) == acc_level);

}

// we generate a block of 8 samples, such as is repeated 256 times in the contents of a block
// for each table 3 instance we have eight audio samples. We consider validity relative to the layout
// defined for table 3 so will ensure that the always zero bits are reset and the counter is consistent
RC_GTEST_PROP(raw_buffer_access, table_3, ( )) {




}





// Some experiments with the named type library
TEST_F(AudioBufferFixture, experiments) {
    using namespace fluent;
    using AudioLevel = NamedType<double, struct AudioLevelTag,
                                 Addable,Comparable,Subtractable, Printable>;


    // assign at construction
    AudioLevel  audio{3.14};

    // reassign
    // audio = 1.23;           //  does not compile cannot assign literal that is not of the audio level type
    audio =  AudioLevel(1.23); //  ok, supports assignment


    // addition
    auto audioAdd = audio + AudioLevel(1.1); // -- does not compile as add is not yet available for the type

    // subtraction
    // audioAdd -= AudioLevel(1.1);    does not compile but ....
    audioAdd = audioAdd - AudioLevel(1.1); // -- does compile

    // try some comparison
    if (audio < AudioLevel(2.22)) {
        audio = AudioLevel(2.22);
    }

    // can we pass it around?
    auto fn = [](AudioLevel al)-> AudioLevel { return al + AudioLevel{3.3}; };
    auto res = fn(AudioLevel(1.0));
    EXPECT_TRUE(res > AudioLevel(4.2) );
    EXPECT_TRUE(res < AudioLevel(4.4) );

    //auto log_res = log(AudioLevel(1.0)); // only possible in this way if we enable implicit conversion to float in the skill set


    // is the size the same as the underlying type
    EXPECT_EQ(sizeof(AudioLevel) , sizeof(double));

}




// constructs and tear downs for the fixtures used in the test
AudioBufferFixture::AudioBufferFixture() {}

AudioBufferFixture::~AudioBufferFixture() {}
