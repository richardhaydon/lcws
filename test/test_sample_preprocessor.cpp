#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>

#include <cstdint>
#include <cstring>

#include "buffer.hpp"
#include "functional"
#include "lcws_constants.hpp"
#include "named_type.hpp"
#include "preprocessor/sample_preprocessor.hpp"
#include "raw_buffer.hpp"
#include "spdlog/spdlog.h"
#include <algorithm>
#include <fstream>
#include <streambuf>
#include <vector>

#include <lcws_constants.hpp>

#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

using testing::_;

// some expected parameters for the configuration
namespace {

#define PI 3.14159265359

template <typename T, unsigned B> inline T tsignextend(const T x) {
  struct {
    T x : B;
  } s;
  return s.x = x;
}

const float normalization_factor = 131072.0f; /* 0x20000 */

int32_t sign_extend18(uint32_t u) { return tsignextend<int32_t, 18>(u); }

signed int sign_extend(const unsigned int value, const unsigned int width) {
  const signed int m = 1U << (width - 1);
  const signed int bits = value ^ m;
  return bits - m;
}

int convert_sample_audio_v2(uint8_t c0, uint8_t c1, uint8_t c2) {
  const unsigned int raw =
      static_cast<unsigned int>((c2 >> 6) | (c1 << 2) | (c0 << 10));
  const int signed_value = sign_extend(raw, 18);
  return signed_value;
}

template <typename T>
void write(std::ofstream& stream, const T& t) {
    stream.write((const char*)&t, sizeof(T));
}

} // namespace

// Create a test fixture to handle common setup and teardown between the tests
class SamplePreprocessorFixture : public ::testing::Test {
public:
  SamplePreprocessorFixture() {
    // points to folder of samples withing the project
    fs::path samples_path;

    // common setups for the fixture, the path for the test samples
    fs::path test_file(__FILE__);
    samples_path = test_file.parent_path().append("samples");

    for (auto &p : fs::directory_iterator(samples_path)) {
      m_samples_library.push_back(p);
    }
    spdlog::info("Sample files count {}", m_samples_library.size());
  }
  ~SamplePreprocessorFixture() {}

  // The sample preprocessor that the tests will use
  SamplePreprocessor m_preprocessor;

  // all the sample files we have identified in our samples folder
  std::vector<fs::path> m_samples_library;
  std::vector<int> m_signed_raw{};
  std::vector<float> m_float_raw{};

};

RC_GTEST_PROP(raw_buffer_access, raw_bitmanipulation, ()) {

  const uint32_t i = *rc::gen::inRange(0, (1 << 18) - 1);

  int lcws_ext = sign_extend(i, 18);
  int pre_ext = sign_extend18(i);

  if (lcws_ext != pre_ext) {
    spdlog::warn("Failed in sample manipulation");
  }

  RC_ASSERT(lcws_ext == pre_ext);
}

TEST_F(SamplePreprocessorFixture, lcws_raw_verification) {
    int samplesize          = 16;
    unsigned int samplerate = 44100;
    int duration            = 3; // seconds
    int channels            = 1;
    //long samples            = 4096;
    long samples            = samplerate * duration; // per channel
    long total_samples      = samples * channels; // all channels

    // Create test input buffer
    char input[total_samples * samplesize/8];

    // Initialize test buffer with zeros
    for (int i=0;i<total_samples * samplesize/8;++i){
        input[i]=0;
    }

    // Create test input
    int total_index=0;
    char graph[20];
    for (long i=0;i<samples;++i){
        // Channel 1: 440 Hz sinus
        int sinus    = .8*0x8000*sin(2*PI*440*i/samplerate); //  amplitude = 0.8 * max range; max range = 0x8000 = 32768 ( max value for 16 Bit signed int )


        // Little Endian: BUG HERE?
        input[total_index] =  sinus & 0xFF;             // LoByte
        //input[total_index+1] = (sinus & 0xFF00) >> 8; // HiByte
        input[total_index+1] = (sinus >> 8) & 0xFF;     // HiByte
        total_index += 2;

    }

    // Export raw audio data to file
    std::ofstream fout("sine_char16bit.raw");
    if (fout.is_open()){
        long idx = 0;
        for(long i = 0; i < samples; ++i){
            fout << input[idx] << input[idx+1]; idx+=2;
        }
        fout.close();
    }

}



// Verify the equivalence of the lcws raw buffer processing with full instrem
// processing of the audio
//TEST_F(SamplePreprocessorFixture, lcws_raw_verification) {
//  lcws::raw_buffer rb;

//  auto f = m_samples_library.front();
//  std::ifstream fs;
//  std::cout << " the path is  " << f;
//  f.make_preferred();
//  spdlog::info("Sanity test with {}", f.u8string());
//  fs.open(f.u8string(), std::ios::binary);
//  EXPECT_TRUE(fs.is_open());

//  // populate outside the raw buffer handler
//  fs.clear();
//  fs.seekg(0, std::ios::beg);

//  while (fs.peek() != EOF) {
//    uint8_t p, q, r, s;
//    fs >> p;
//    fs >> q;
//    fs >> r;
//    int csa = convert_sample_audio_v2(p, q, r);
//    if (csa < static_cast<int>(~0x1FFFF) || csa >= static_cast<int>(0x20000)) {
//        assert ("lcws::raw_buffer::convert: Sample out of range");
//    }
//    const float sample = static_cast<float>(csa) / normalization_factor ;
//    //spdlog::info("raw={}fl={}",csa,sample);
//    m_signed_raw.push_back(csa);
//    m_float_raw.push_back(sample);
//  }

//  fs.close();

//  // now the floats to raw audio
//  std::ofstream stream("fexample.raw", std::ios::binary);
//  if (stream.is_open()) {
//      for(auto f:m_float_raw) {
//          write<float>(stream,f);
//      }

//  } else {
//      spdlog::warn("cannot open the out file ");
//  }
//  stream.close();

//}




