/***
*
* Copyright (C) 2012 to the present. WaveTrain Systems AS
*
***/

#include "raw_buffer.h"
#include "internal.h"
#include "logger.h"
#include "accelerometer.h"
#include "Spam_protector.h"
#include <cstring>  // memset
#include <algorithm>
#include <cmath>      /* log10()/sqrt()/fabs() and friends */

namespace {
  constexpr int min_spread = 10;
  constexpr int32_t max_sum = 2048 * 200;
  constexpr double min_level = -100.0;
  constexpr double low_DC_level = -70.0;
  const double level_adjustment = -20.0 * std::log10(128.0 * 1024.0);

  inline signed int sign_extend(const unsigned int value, const unsigned int width)
  {
    // sign extend trick. See https://graphics.stanford.edu/~seander/bithacks.html#VariableSignExtend

    //Create a bitmask for the sign bit
    const signed int m = 1U << (width - 1);
    //XOR bitwise of the mask and the value
    //(we flip the sign bit)
    const signed int bits = value ^ m;
    //Obtain the result
    return bits - m;
    //If the number were positive then we flipped sign bit to 1 and the substraction set it back to 0
    //If the number were negative then we flipped sign bit to 0 and the substraction will propagate sign bit
  }
  inline int convert_sample_audio_v2 (const unsigned char *c)
  {
    const unsigned c0 = static_cast<unsigned>(c [0]);
    const unsigned c1 = static_cast<unsigned>(c [1]);
    const unsigned c2 = static_cast<unsigned>(c [2]);
    const unsigned int raw = static_cast<unsigned int>((c2 >> 6) | (c1 << 2) | (c0 << 10));
    const int signed_value = sign_extend(raw, 18);
    return signed_value;
  }

  inline int convert_sample_audio (const unsigned char *c)
  {
    const unsigned c0 = static_cast<unsigned>(c [0]);
    const unsigned c1 = static_cast<unsigned>(c [1]);
    const unsigned c2 = static_cast<unsigned>(c [2] & 192u);
    const int raw = static_cast<int>((c2 << 8) | (c1 << 16) | (c0 << 24));
    const int signed_value = raw / 16384; /* Shift right 6 + 8 bits using division instead of bitshifting in order to ensure the required sign extension. */
    LCWS_ASSERT(signed_value == convert_sample_audio_v2(c));
    return signed_value;
  }
};

lcws::raw_buffer::raw_buffer ()
{
  ::memset (_buf,0,bytes_per_buffer);
}

void lcws::raw_buffer::revert_sample(unsigned int i,float sample)
{
   const float normalization_factor = 131072.0f; /* 0x20000 */
   if (sample > 1.0f) 
      sample = 1.0f;
   else if (sample < -1.0f)
      sample = -1.0f;

   unsigned int value = static_cast<unsigned int>(sample * normalization_factor);
   unsigned char *c = _buf + i * bytes_per_sample;
   value = value * 16384;
   c[0] = (value >> 24) & 0xff;
   c[1] = (value >> 16) & 0xff;
   c[2] = (value >> 8) & 0xff;
}

void lcws::raw_buffer::revert (buffer &buf) 
{
   for (unsigned int i = 0; i < lcws::buffer::frames; i++) {
     float sample = buf.get()[i];
     revert_sample (i,sample);
  }
}

int lcws::raw_buffer::raw_sample_audio (const unsigned char *c)
{
  return convert_sample_audio (c);
}

unsigned char lcws::raw_buffer::raw_sample_accelerometer (const unsigned char *c)
{
  // Masking away the 2 audio-bits that is part of the byte with the accelerometer data
  return c[2] & 0x3f;
}

float lcws::raw_buffer::convert (buffer &buf, bool pre1dot6) const
{
  LCWS_ASSERT(verify_integrity());
  float *p = buf.get ();
  float sum_of_raw_samples = 0.0f;
  unsigned int number_of_clipped_samples = 0;
  const float normalization_factor = 131072.0f; /* 0x20000 */
  const float clipping_threshold = 0.9999f * normalization_factor;

  /*
  Convert the raw samples to floating point and compute the total sum of the
  samples in preparation for computing the dc component, which will later be
  removed in a second step.
  */
  for (unsigned int i = 0; i < lcws::buffer::frames; i++) {
    // Convert audio data
    const int raw_sample = convert_sample_audio (_buf + i * bytes_per_sample);
    if (raw_sample < static_cast<int>(~0x1FFFF) || raw_sample >= static_cast<int>(0x20000)) {
      LCWS_TRACE ("lcws::raw_buffer::convert: Sample out of range (" << std::hex << raw_sample << ")" << std::endl);
    }
    const float sample = static_cast<float>(raw_sample);
    sum_of_raw_samples += sample;
    p [i] = sample;
    if (fabs (sample)> clipping_threshold) { 
      number_of_clipped_samples++;
    }

    // Convert accelerometer and temperature data
    lcws::accelerometer_data &accdata = buf.get_accelerometer_data(i / lcws::buffer::acc_temp_frame_size);
    const bool ok = accdata.update(i,raw_sample_accelerometer(_buf + i * bytes_per_sample), pre1dot6);
    (void)ok; // not using return value 'ok' by intention so therefore void it.
  }

  /*
  (buf) has now been filled in with the floating point representation of the
  raw samples. Now compute the raw dc component of those samples.
  
  Only compute and remove the DC component if there are no clipped samples, thus set DC component to 0 if there are clipped samples.
  If there are clipped samples the signal is saturating and the DC component will be calculated incorrectly 
  The DC component is negligible in such cases and can be ignored
  */
  const float mean_average = (sum_of_raw_samples / lcws::buffer::frames);
  const float raw_dc_component = (number_of_clipped_samples == 0) ? mean_average : 0;

  /*
  Now subtract the dc component and then normalize the samples
  between -1 and 1.
  */
  float sum_of_raw_samples_without_dc_component = 0;
  float standard_deviation = 0;
  for (unsigned int i = 0; i < lcws::buffer::frames; i++) {
    const float diff = (p[i] - mean_average) / normalization_factor;
    standard_deviation += (diff * diff);
    const float raw_sample_without_dc_component = p [i] - raw_dc_component;
    sum_of_raw_samples_without_dc_component += raw_sample_without_dc_component;
    float normalized_sample = raw_sample_without_dc_component / normalization_factor;
    // make sure we do not go outside range, can happen with an incorrect dc component
    normalized_sample = (normalized_sample < -1) ? -1: ((normalized_sample > 1) ? 1 : normalized_sample);
    p [i] = normalized_sample;
  }
  standard_deviation = sqrt(standard_deviation / lcws::buffer::frames);
  buf.set_stddev(standard_deviation);
  return (raw_dc_component);
}

bool lcws::raw_buffer::read (std::ifstream &file)
{
  char *data = reinterpret_cast<char *>(get_bytes());
  file.read (data,bytes_per_buffer);
  return true;
}

bool lcws::raw_buffer::write (std::ofstream &file)
{
  const unsigned char *data = get_bytes_readonly();
  file.write (reinterpret_cast<const char *>(data),bytes_per_buffer);
  return true;
}


bool lcws::raw_buffer::verify_integrity() const
{
  static lcws::Spam_protector spam_protector{ 10, 60000ull, 3600000ull };

  unsigned int last_zero_or_counter = 0;
  bool byte_counter_implemented = false;

  for (unsigned int i = 0; i < lcws::buffer::frames; i += samples_per_counter) {
    //Get the always zero field
    const unsigned int always_but_after_reset_zero = 
      (static_cast<unsigned>(_buf[i * bytes_per_sample + byte_offset_always_but_after_reset_zero]) ) & always_but_after_reset_zero_mask;
    //If the always zero bits are not actually 0 the integrity is broken
    if ( always_but_after_reset_zero && !( ( always_but_after_reset_zero == first_buffer_after_sensor_reset_marker ) && ( i == 0 ) ) )
    {
      {
        std::ostringstream message;
        message << "lcws::raw_buffer::verify_integrity always but after reset zero bit mismatch.";
        spam_protector.log(lcws::logger::lp_alert, message.str().c_str());
      }

      return false;
    }
    //Get byte counter bits in latest sensor firmware version or zeroes in previous ones
    const unsigned int zero_or_counter = (static_cast<unsigned>(_buf[i * bytes_per_sample + byte_offset_zero_or_counter]) )& zero_or_counter_mask;
    //In the first iteration we only take the value and go to next iteration inmediately
    if (i == 0)
    {
      last_zero_or_counter = zero_or_counter;
      continue;
    }
    //In the second iteration we decide if byte counter feature is implemented in this sensor
    if (i == samples_per_counter)
    {
      if ((last_zero_or_counter != 0) || (zero_or_counter != 0))
      {
        byte_counter_implemented = true;
      }
    }

    if (byte_counter_implemented)
    {
      //In case we had detected byte counter feature we check sequence
      if (!check_counter(last_zero_or_counter, zero_or_counter))
      {
        {
          std::ostringstream message;
          message << "lcws::raw_buffer::verify_integrity byte counter mismatch.";
          spam_protector.log(lcws::logger::lp_alert, message.str().c_str());
        }

        return false;
      }
    }
    else
    {
      //In case we have always 0 we test just that
      if (zero_or_counter)
      {
        {
          std::ostringstream message;
          message << "lcws::raw_buffer::verify_integrity no byte counter zeroed field mismatch.";
          spam_protector.log(lcws::logger::lp_alert, message.str().c_str());
        }
        return false;
      }
    }
    last_zero_or_counter = zero_or_counter;
  }

  const auto first_raw_sample = convert_sample_audio(_buf);
  int64_t sum_samples = first_raw_sample;
  int64_t sum_squared_samples = int64_t(first_raw_sample) * int64_t(first_raw_sample);
  auto max_value = first_raw_sample;
  auto min_value = first_raw_sample;
  for (unsigned int i = 1; i < lcws::buffer::frames; ++i) {
    const auto sample = convert_sample_audio(_buf + i * bytes_per_sample);
    max_value = std::max(max_value, sample);
    min_value = std::min(min_value, sample);
    sum_samples += sample;
    sum_squared_samples += int64_t(sample) * int64_t(sample);
  }

  double level_dBFS = (10.0 * std::log10(double(sum_squared_samples) / double(lcws::buffer::frames) -
    (double(sum_samples) / double(lcws::buffer::frames)) * (double(sum_samples) / double(lcws::buffer::frames))))
    + level_adjustment;

  if ((max_value - min_value) < min_spread)
  {
    {
      std::ostringstream message;
      message << "lcws::raw_buffer::verify_integrity spread " <<
        max_value - min_value << ", below minimum.";
      spam_protector.log(lcws::logger::lp_alert, message.str().c_str());
    }
    return false;
  }

  if (level_dBFS < min_level)
  {
    {
      std::ostringstream message;
      message << "lcws::raw_buffer::verify_integrity level " <<
        level_dBFS << ", below minimum.";
      spam_protector.log(lcws::logger::lp_alert, message.str().c_str());
    }
    return false;
  }

  if ( level_dBFS < low_DC_level && (llabs(sum_samples) > max_sum))
  {
    {
      std::ostringstream message;
      message << "lcws::raw_buffer::verify_integrity high DC offset, spread " <<
        max_value - min_value << ", abs(sum_samples) " << llabs(sum_samples) << 
        ", DC offset " << (double(sum_samples) / double(lcws::buffer::frames));
      spam_protector.log(lcws::logger::lp_alert, message.str().c_str());
    }
    return false;
  }
  return true;
}

bool lcws::raw_buffer::first_buffer_after_sensor_reset() const
{
  const unsigned int always_but_after_reset_zero = 
    (static_cast<unsigned>(_buf[byte_offset_always_but_after_reset_zero]) ) & always_but_after_reset_zero_mask;

  return always_but_after_reset_zero == first_buffer_after_sensor_reset_marker;
}

bool lcws::raw_buffer::check_counter(unsigned int previous, unsigned int next)
{
  auto previous_incremented = previous + (bytes_per_sample * samples_per_counter);
  auto masked = previous_incremented & zero_or_counter_mask;

  return (next == masked);
}

