# Support for generating code coverage reports with gcovr.
# Options:
# GENERATE_COVERAGE - If the code should be built with coverage metrics enabled. Default yes.
# Variables:
# COVERAGE_EXCLUDES - List of paths to exclude from the coverage measurement.

option(GENERATE_COVERAGE "Build with coverage metrics" ON)

list(APPEND COVERAGE_EXCLUDES ${CMAKE_SOURCE_DIR}/test/googletest ${CMAKE_SOURCE_DIR}/test/rapidcheck ${CMAKE_SOURCE_DIR}/libs  ${CMAKE_SOURCE_DIR}/src/serialization/cereal ${CMAKE_BINARY_DIR})
foreach(exclude ${COVERAGE_EXCLUDES})
    set(excludes ${excludes} --exclude ${exclude})
endforeach()

if(GENERATE_COVERAGE AND CMAKE_BUILD_TYPE STREQUAL "Debug" AND NOT MSVC AND NOT CMAKE_CROSSCOMPILING)
    add_definitions("--coverage")
    set(CMAKE_SHARED_LINKER_FLAGS  "${CMAKE_SHARED_LINKER_FLAGS} --coverage" )
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")

    # if we've built with Clang, we need to use llvm-cov to process files, otherwise just use gcov
    if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
        set(GCOV_ENV "llvm-cov gcov")
    else()
        set(GCOV_ENV "gcov")
    endif()

    find_package(gcovr)
    if(GCOVR_FOUND)
        message(STATUS "The coverage command is " ${GCOVR_EXECUTABLE} )
        # only make a coverage target if we're the top-level target
        if("${PROJECT_NAME}" STREQUAL "${CMAKE_PROJECT_NAME}")
            add_custom_target(coverage
                              COMMAND ${CMAKE_COMMAND} -E make_directory coverage_reports
                              COMMAND ${CMAKE_COMMAND} -E env GCOV=${GCOV_ENV} ${GCOVR_EXECUTABLE}
                                      -r ${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR}
                                      --print-summary --html --html-details
                                      -o coverage_reports/coverage_report_detailed.html
                                      ${excludes}
                                      --delete
                              WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                              COMMENT "Generating coverage data using gcovr"
                              VERBATIM)
        endif()
    else()
        message(STATUS "Failed to find gcovr module. Coverage report not generated")
    endif()
endif()

