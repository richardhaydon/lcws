# - Find gcovr
# Will define:
#
# GCOVR_EXECUTABLE - the gcovr executable
#

INCLUDE(FindPackageHandleStandardArgs)

FIND_PROGRAM(GCOVR_EXECUTABLE gcovr)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(gcovr DEFAULT_MSG GCOVR_EXECUTABLE)

# only visible in advanced view
MARK_AS_ADVANCED(GCOVR_EXECUTABLE)
