# Adds a target to run clang-format on all enabled targets.
#
# Please note that clang_format() must be called for the targets to format.
#
# Example:
#
#     add_library(foo ${FOO_HEADERS} ${FOO_SOURCES})
#     clang_format(foo)
#

# Options
option(FORMAT_CODE "Automatically reformat code for enabled targets." OFF)

function(clang_format _target)
    if(NOT TARGET ${_target})
        message(FATAL_ERROR "clang_format can only be used on targets (got ${_target}.)")
    endif()

    # should we try to format the code?
    if(FORMAT_CODE)
        # figure out which sources we should apply clang-format to.
        get_target_property(_format_sources ${_target} SOURCES)
        get_target_property(_binary_dir ${_target} BINARY_DIR)

        # loop through all sources and add a clang format command for each, with a phony file
        # to timestamp for the format target later
        set(_sources)
        foreach(_source ${_format_sources})
            if(NOT TARGET ${_source})
                get_filename_component(_source_file ${_source} NAME)
                get_source_file_property(_source_location ${_source} LOCATION)

                set(_format_file ${_target}_${_source_file}.format)
                set(_cmd_list "clang-format" "-style=file" "-i" "${_source_location}")

                add_custom_command(OUTPUT ${_format_file}
                    DEPENDS ${_source}
                    COMMENT "clang-format ${_source}"
                    COMMAND ${_cmd_list}
                    COMMAND ${CMAKE_COMMAND} -E touch ${_format_file}
                    )

                list(APPEND _sources ${_format_file})
            endif()
        endforeach()

        # if the target had any sources, add the custom format target and add it as a dependency
        # to the given target.
        if(_sources)
            add_custom_target(${_target}_format
                SOURCES ${_sources}
                COMMENT "clang-format for target ${_target}"
                )
            add_dependencies(${_target} ${_target}_format)
        endif()
    endif()

endfunction()
