include(Clang-Format)

# add a test with dependencies on gtest and add it to CTest.
# add_gtest(<name> [DEPENDENCIES ...] [INCLUDES ...] [LIBRARIES ...] SOURCES source1 [source2 ...])
# where DEPENDENCIES signal one or more dependencies, INCLUDES signal one or more include
# directories/files, LIBRARIES signal one or more libraries to link to and SOURCES indicate
# source files. One or more source files must be added.
function(add_gtest name)
    set(options)
    set(onevalueargs)
    set(multivalueargs DEPENDENCIES LIBRARIES INCLUDES SOURCES)
    cmake_parse_arguments(ADD_GTEST "${options}" "${onevalueargs}" "${multivalueargs}" ${ARGN})
    set(test_name "test_${name}")

    add_executable(${test_name} ${ADD_GTEST_SOURCES})
    target_include_directories(${test_name} BEFORE PRIVATE ${ADD_GTEST_INCLUDES})
    add_dependencies(${test_name} gtest gmock rapidcheck rapidcheck_gtest ${ADD_GTEST_DEPENDENCIES})
    target_link_libraries(${test_name} gtest_main gtest gmock rapidcheck_gtest
                                       ${ADD_GTEST_DEPENDENCIES} ${ADD_GTEST_LIBRARIES})

    add_test(${name} ${test_name})
    clang_format(${test_name})
endfunction()

# add a benchmark with dependencies on the benchmark library and add it to CTest.
# add_bench(<name> [DEPENDENCIES ...] [INCLUDES ...] [LIBRARIES ...]
#                  SOURCES source1 [source2 ...])
# where DEPENDENCIES signal one or more dependencies, INCLUDES signal one or more include
# directories/files, LIBRARIES signal one or more libraries to link to and SOURCES indicate
# source files. One or more source files must be added.
function(add_bench name)
    set(options)
    set(onevalueargs)
    set(multivalueargs DEPENDENCIES LIBRARIES INCLUDES SOURCES)
    cmake_parse_arguments(ADD_BENCHMARK "${options}" "${onevalueargs}" "${multivalueargs}" ${ARGN})
    set(benchmark_name "bench_${name}")

    add_executable(${benchmark_name} ${ADD_BENCHMARK_SOURCES})
    target_include_directories(${benchmark_name} BEFORE PRIVATE ${ADD_BENCHMARK_INCLUDES})
    add_dependencies(${benchmark_name} gtest gmock benchmark ${ADD_BENCHMARK_DEPENDENCIES})
    target_link_libraries(${benchmark_name} gtest gmock benchmark ${ADD_BENCHMARK_DEPENDENCIES}
                                            ${ADD_BENCHMARK_LIBRARIES})

    clang_format(${benchmark_name})
endfunction()
